﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _playerText;
    [SerializeField] private TextMeshProUGUI _playerDamageText;
    [SerializeField] private TextMeshProUGUI _playerLivesText;
    [SerializeField] private Image _playerIcon;

    public void SetPlayer(int index) { _playerText.text = "Player " + index;  }
    public void SetDamage(int damage) { _playerDamageText.text = damage + "%"; }
    public void SetLives(int lives) { _playerLivesText.text = "Lives " + lives; }
    public void SetIcon(Sprite icon) { _playerIcon.sprite = icon; }    
}
