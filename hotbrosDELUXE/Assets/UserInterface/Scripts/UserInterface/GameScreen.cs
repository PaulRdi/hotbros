﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScreen : MonoBehaviour
{
    [SerializeField] public PlayersPanel _playersPanel;

    public void Initialize(int playerCount, int lives)
    {
        _playersPanel.Initialize(playerCount, lives);
    }

    public void Show() { gameObject.SetActive(true); }
    public void Hide() { gameObject.SetActive(false); }
}