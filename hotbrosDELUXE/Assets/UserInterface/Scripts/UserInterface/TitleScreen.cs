﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class TitleScreen : MonoBehaviour
{
    private Action _onPlay;

    public void OnPlayButton()
    {
        if (_onPlay != null)
            _onPlay();
        else
            Debug.LogWarning("OnPlay is null moron!");
    }    

    public void Show() { gameObject.SetActive(true); }
    public void Hide() { gameObject.SetActive(false); }

    public Action OnPlay { set { _onPlay = value; } }
}
