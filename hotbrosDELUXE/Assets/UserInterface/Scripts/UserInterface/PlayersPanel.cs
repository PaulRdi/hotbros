﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayersPanel : MonoBehaviour
{
    public GameObject itemPrefab;
    public List<PlayerItem> _items;

    public void Initialize(int playerCount, int lives)
    {
        _items = new List<PlayerItem>();

        for (int i = 0; i < playerCount; i++)
        {
            GameObject itemObject = GameObject.Instantiate(itemPrefab, gameObject.transform);
            RectTransform itemTransform = itemObject.GetComponent<RectTransform>();
            PlayerItem item = itemObject.GetComponent<PlayerItem>();

            itemTransform.anchoredPosition = new Vector2(i * 150 + 50f, 0f);
            item.SetPlayer(i + 1);
            item.SetDamage(0);
            item.SetLives(lives);

            _items.Add(item);
        }
    }

    public void AssignPlayerDamage(int playerIndex, int damage) { _items[playerIndex].SetDamage(damage); }
    public void AssignPlayerLives(int playerIndex, int lives) { _items[playerIndex].SetLives(lives); }

    public static void SetPlayerDamage(int playerIndex, int damage)
    {
        Instance.AssignPlayerDamage(playerIndex, damage);
    }

    public static void SetPlayerLives(int playerIndex, int lives)
    {
        Instance.AssignPlayerLives(playerIndex, lives);
    }




    private static PlayersPanel _instance;
    private static PlayersPanel Instance
    {

        get
        {
            if (_instance == null) _instance = GameObject.FindObjectOfType<PlayersPanel>();
            return _instance;
        }
    }
}
