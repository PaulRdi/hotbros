﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UserInterface : MonoBehaviour
{
    public static event Action GameOver;

    public TitleScreen _titleScreen;
    public GameScreen _gameScreen;

    public bool gameRunning
    {
        get
        {
            return _gameRunning;
        }
    }
    bool _gameRunning;

    public static UserInterface instance
    {
        get
        {
            return _instance;
        }
    }
    static UserInterface _instance;

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Initialize(Play);
            DontDestroyOnLoad(this.gameObject);
            _instance = this;
            _gameRunning = false;
            PlayerController.NoLives += PlayerController_NoLives;
        }
    }

    private void PlayerController_NoLives(PlayerController obj)
    {
        _gameRunning = false;
        ShowTitleScreen();
        if (GameOver != null)
            GameOver();
    }

    void Play()
    {
        ShowGameScreen();
        _gameRunning = true;
    }
    public void Initialize(Action onPlay)
    {
        _gameScreen.Initialize(2, 3);
        _titleScreen.OnPlay = onPlay;

        ShowTitleScreen();
    }

    public void ShowTitleScreen()
    {
        HideAllScreens();
        _titleScreen.Show();        
    }

    public void ShowGameScreen()
    {
        HideAllScreens();
        _gameScreen.Show();
    }

    private void HideAllScreens()
    {
        _titleScreen.Hide();
        _gameScreen.Hide();
    }
}