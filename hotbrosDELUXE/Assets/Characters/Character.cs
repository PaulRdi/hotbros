﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    private const string IDLE = "Idle";
    private const string RUN = "Run";
    private const string BEGIN_JUMP = "BeginJump";
    private const string END_JUMP = "EndJump";
    [SerializeField] private Animator _animator;

    public void Idle() { ResetTriggers(); _animator.SetTrigger(IDLE); }
    public void Run() { ResetTriggers(); _animator.SetTrigger(RUN); }
    public void BeginJump() { ResetTriggers(); _animator.SetTrigger(BEGIN_JUMP); }
    public void EndJump() { ResetTriggers(); _animator.SetTrigger(END_JUMP); }
    public void StartAttack() { ResetTriggers(); _animator.SetLayerWeight(1, 1); }
    public void EndAttack() { ResetTriggers(); _animator.SetLayerWeight(1, 0); }

	public void TurnLeft(){
		transform.rotation = Quaternion.Euler(0,180,0);
	}
	public void TurnRight(){
		transform.rotation = Quaternion.Euler(0,0,0);
	}

    private void ResetTriggers()
    {
        _animator.ResetTrigger(IDLE);
        _animator.ResetTrigger(RUN);
        _animator.ResetTrigger(BEGIN_JUMP);
        _animator.ResetTrigger(END_JUMP);
    }
}
