﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCollisionTest : MonoBehaviour {

    int i = 0;
    float t = 0;
    private void Update()
    {
        if (t > 1.0f)
        {
            t = 0f;
            Debug.Log(i);
        }
        t += Time.deltaTime;
    }
    private void OnParticleCollision(GameObject other)
    {
        i++;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        i++;
    }
}
