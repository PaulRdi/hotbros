﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class LiquidAnchor : MonoBehaviour {

    public float flyTime;
    public event Action<LiquidAnchor> destroyAction;

    float timeFlown;
	
    public void Init(float ft, Action<LiquidAnchor> ac, Vector2 flyForce)
    {
        GetComponent<Rigidbody2D>().AddForce(flyForce);
        flyTime = ft;
        destroyAction += ac;
        timeFlown = 0;
    }

	void Update () {

        if (!UserInterface.instance.gameRunning) return;

        if (timeFlown > flyTime)
        {
            CleanupAnchor();
        }
        timeFlown += Time.deltaTime;
	}

    void CleanupAnchor()
    {
        destroyAction(this);
        Destroy(this.gameObject);
    }
}
