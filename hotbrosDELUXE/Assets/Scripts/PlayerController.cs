﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlayerController : MonoBehaviour {

    public static event Action<PlayerController> NoLives;

	public int thrust = 20;
	public int walkSpeed = 3;
	public int lives=5;
    int maxLives;
	Rigidbody2D rb;
	CircleCollider2D cc;
	bool midJumptriggered=false;
	public Vector2 spawnPostion;
	public int playerNumber=0;
	KeyCode up=KeyCode.W;
	KeyCode left=KeyCode.A;
	KeyCode right=KeyCode.D;
	KeyCode spray=KeyCode.Q;
	public Text countText;
	public bool dircetionLeft=true;
	bool injump=false;
	LiquidSpawner ls;
	ParticleGetHit pgh;
	Character c;
	Vector2 velocityOld=new Vector2(0,0);


	// Use this for initialization
	void Start () {
		ls = gameObject.GetComponentInChildren<LiquidSpawner>();
		pgh = gameObject.GetComponentInChildren<ParticleGetHit>();
		c = gameObject.GetComponentInChildren<Character>();
		rb = GetComponent<Rigidbody2D>();
		cc = GetComponent<CircleCollider2D>();
        maxLives = lives;
        UserInterface.GameOver += UserInterface_GameOver;
		spawnPostion = this.transform.position;
		if(this.playerNumber==1){
			this.up=KeyCode.UpArrow;
			this.left=KeyCode.LeftArrow;
			this.right=KeyCode.RightArrow;
			this.spray=KeyCode.Minus;
		}
		respwn();
		
	}

    private void UserInterface_GameOver()
    {
        lives = maxLives;
        transform.position = spawnPostion;
       
		respwn();
    }

    // Update is called once per frame
    void Update () {
        if (!UserInterface.instance.gameRunning) return;

		if(velocityOld.y>0 && rb.velocity.y<=0){
			c.EndJump();
		}

		if(pgh.percent>10000.0){
			this.lives=lives-1;
			respwn();
		}
		UserInterface.instance._gameScreen._playersPanel.AssignPlayerDamage(playerNumber,(int)(pgh.percent*100));

		GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("deadzone");
		foreach (GameObject go in gos) {
				if(cc.IsTouching(go.GetComponent<Collider2D>())){
					this.lives=lives-1;
					respwn();
				}
			 }
			 gos = GameObject.FindGameObjectsWithTag("floor");
			foreach (GameObject go in gos) {
				Collider2D c2=go.GetComponent<Collider2D>();
				if(c2!=null&&injump&&cc.IsTouching(c2)){
					c.Idle();
				}
			 }
		 if (Input.GetKeyDown(this.spray))
        {
			ls.Toggle(true);
			c.StartAttack();
		}
		 if (Input.GetKeyUp(this.spray))
        {	
			ls.Toggle(false);
			c.EndAttack();
		}

		 if (Input.GetKey(left))
        {
			c.Run();
			c.TurnLeft();
			ls.ResetRotation();
			dircetionLeft=true;
			rb.velocity=new Vector2(-walkSpeed,rb.velocity.y)+pgh.waterForce;
		}
		if (Input.GetKeyDown(left))
        {
		
		}
		 if (Input.GetKeyUp(left))
        { 
			c.Idle();
			rb.velocity=new Vector2(0,rb.velocity.y);
		}
		if (Input.GetKey(right))
        {
			c.Run();
			c.TurnRight();
			ls.ResetRotation();
			dircetionLeft=false;
			rb.velocity=new Vector2(walkSpeed,rb.velocity.y);
		}
		if (Input.GetKeyDown(right))
        {
			rb.velocity=new Vector2(0,rb.velocity.y);
			
		}
		if (Input.GetKeyUp(right))
        {
			c.Idle();
			rb.velocity=new Vector2(0,rb.velocity.y);
		}
		if (Input.GetKeyDown(up))
        {	injump=true;
			c.BeginJump();
			ls.SetRotation(180);
			bool jumped=false;	
        	gos = GameObject.FindGameObjectsWithTag("floor");
			 foreach (GameObject go in gos) {
				if(cc.IsTouching(go.GetComponent<Collider2D>())&&!jumped){
					jump();
				 	midJumptriggered=false;
					 jumped=true;
					 ls.EmitOverTime(0.2f,20,emmitEnded);
				}
			 }
			 if(!midJumptriggered&&!jumped){
					midJumptriggered=true;
					jump();
					 jumped=true;
					 ls.EmitOverTime(0.2f,20,emmitEnded);
				}
        }
		velocityOld=rb.velocity;
	}
	void emmitEnded(){
	//	ls.ResetRotation();
	}
	void respwn(){
        if (lives >= 1)
        {
            rb = GetComponent<Rigidbody2D>();
            rb.velocity = new Vector2(0, 0);
            this.transform.position = spawnPostion;
            pgh.ResetGetHit();
            pgh.Reset();
			 c.Idle();
			ls.Toggle(false);
            //countText.text = lives + " Lives";
			UserInterface.instance._gameScreen._playersPanel.AssignPlayerLives(playerNumber,lives);
        }
        else
        {
            if (NoLives != null)
                NoLives(this);
        }
	}
	void jump(){
			rb.velocity=new Vector2(rb.velocity.x,thrust);
	}
}
