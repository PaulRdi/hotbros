﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ParticleGetHit : MonoBehaviour {

    public const float PERCENT_DAMAGE_PER_HIT = 0.005f;
    public bool gotHit;
    public event Action<GameObject> GotHit;
    Rigidbody2D myRigid;
    public float knockbackStrength = 0.010f;
    public float percent;
    Vector2 knockbackDir;
    Vector2 cachedOpposingForce;
    public float gotHitForceCooldownMod = 3.0f;
    public Vector2 waterForce
    {
        get {return cachedOpposingForce; }
    }
    [SerializeField] float gotHitCooldown = 0.1f;
    float counter;

    private void Awake()
    {
        myRigid = GetComponent<Rigidbody2D>();
        gotHit = false;
        counter = 0.0f;
        percent = 0.01f;
        cachedOpposingForce= new Vector2(0,0);
    }

    void Update()
    {
        if (!UserInterface.instance.gameRunning) return;

        if (counter > gotHitCooldown)
        {
            ResetGetHit();
        }

        if (gotHit)
        {
            counter += Time.deltaTime; 
            cachedOpposingForce = (knockbackDir * knockbackStrength * percent)* (1 - counter / (gotHitCooldown * gotHitForceCooldownMod));
        }           
    }
 

    public void ResetGetHit()
    {
        counter = 0.0f;
        gotHit = false;
        cachedOpposingForce = new Vector2(0,0);
    }
    public void Reset()
    {
        percent = 0.01f;
    }
    void GetHit(GameObject other)
    {
        knockbackDir = (transform.position - other.transform.position).normalized;
        myRigid.AddForce(knockbackDir * knockbackStrength * percent, ForceMode2D.Impulse);

        percent += PERCENT_DAMAGE_PER_HIT;
        if (GotHit != null)
            GotHit(other);

    }


    private void OnParticleCollision(GameObject other)
    {
        if (!gotHit)
        {
            gotHit = true;
            GetHit(other);
        }
    }
}
