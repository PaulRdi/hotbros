﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiquidSpawner : MonoBehaviour {

    [SerializeField]
    AnimationCurve emissionAmountCurve;

    ParticleSystem waterParticleSystem;
    PlayerController plrController;
    float zDegrees, initZ;
    private Action emissionOverTimeFinishedAction;

    private void Awake()
    {
        initZ = transform.rotation.eulerAngles.z;
        zDegrees = initZ;
        waterParticleSystem = GetComponent<ParticleSystem>();
        plrController = transform.parent.GetComponent<PlayerController>();
        waterParticleSystem.Stop();
    }

    public void EmitOverTime(float time, int maxAmount, System.Action emissionFinished)
    {
        emissionOverTimeFinishedAction = emissionFinished;
        StartCoroutine(EmissionOverTimeRoutine(time, maxAmount));
    }
    IEnumerator EmissionOverTimeRoutine(float time, int maxAmount)
    {
        for (float t = 0; t < time; t += Time.deltaTime)
        {
            EmitParticles((int)(emissionAmountCurve.Evaluate(t) * maxAmount));
            yield return null;
        }

        ResetRotation();
        if (emissionOverTimeFinishedAction != null)
            emissionOverTimeFinishedAction();
    }
    public void EmitParticles (int amount)
    {
        waterParticleSystem.Emit(amount);
    }
    public void ResetRotation()
    {
        zDegrees = initZ;
    }
    /// <summary>
    /// clamps Z-Angle of particle system between 0 & 180°
    /// </summary>
    /// <param name="angle"></param>
    public void SetRotation(float angle)
    {
        zDegrees = Mathf.Clamp(angle, 0f, 180f);
    }
    private void Update()
    {
        if (!UserInterface.instance.gameRunning) return;

        transform.rotation = Quaternion.Euler(0, (plrController.dircetionLeft ? 0 : 1) * 180, zDegrees);
    }
    public void Toggle(bool on = true)
    {
        if (on)
        {
            waterParticleSystem.Play();
        }
        else
            waterParticleSystem.Stop();

    }


    //   [SerializeField] float physicsSpawnInterval = 0.4f;
    //   [SerializeField] float strength = 400f;
    //   [SerializeField] GameObject anchorPrefab;
    //   [SerializeField] float maxFlyTime = 2.0f;
    //   [SerializeField] Vector2 flyForce;

    //   List<Transform> myAnchors;

    //   EdgeCollider2D myCol;


    //   void Start () {
    //       myAnchors = new List<Transform>();
    //       myCol = GetComponent<EdgeCollider2D>();
    //       myAnchors.Add(transform);
    //       StartCoroutine(SpawnPhysicsAnchors());

    //}

    //void Update () {

    //       UpdatePoints();

    //}
    //   void UpdatePoints()
    //   {
    //       Vector2[] pts = new Vector2[myAnchors.Count];
    //       for (int i = 0; i < pts.Length; i++)
    //       {
    //           pts[i] = myAnchors[i].position;
    //       }
    //       myCol.points = pts;
    //   }

    //   IEnumerator SpawnPhysicsAnchors()
    //   {
    //       while (true)
    //       {
    //           GameObject tmp = Instantiate(anchorPrefab);
    //           tmp.gameObject.GetComponent<LiquidAnchor>().Init(maxFlyTime, AnchorDestroyed, flyForce * strength);
    //           myAnchors.Insert(myAnchors.Count - 1,tmp.transform);
    //           yield return new WaitForSeconds(physicsSpawnInterval);
    //       }
    //   }

    //   private void AnchorDestroyed(LiquidAnchor obj)
    //   {
    //       myAnchors.Remove(obj.transform);
    //       obj.destroyAction -= AnchorDestroyed;
    //   }
}
